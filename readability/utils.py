import re

from typing import Any, List

import mwparserfromhell  # type: ignore
import nltk  # type: ignore

from readability.constants import DICT_ABBREVIATIONS, DICT_LANG_NLTK


def wikitext2text(wikitext: str) -> str:
    # we select only the lead section
    first_section = re.search('={2,}.+?={2,}', wikitext)
    if first_section:
        wikitext_sec = wikitext[:first_section.span()[0]]
    else:
        wikitext_sec = wikitext

    # we replace <ref>-tags
    filter_wikitext = re.sub(r"<\s*ref.*?\n?(<\s*/ref\s*>|/\s*>)", "", wikitext_sec)
    wikicode = mwparserfromhell.parse(filter_wikitext)

    text = wikicode.strip_code()
    return text


def text2sentences(text: str, lang: str = "en") -> List[str]:
    # map to nltk's language (default: english)
    lang_nltk = DICT_LANG_NLTK.get(lang, "english")

    # full stop bengali
    text = text.replace("।", ".\n")
    # full stop armenian (this is not the same as colon)
    text = text.replace("։", ".\n")

    sentences = []
    # loading the sentence-tokenizer
    try:
        sentence_tokenizer = nltk.data.load(f"tokenizers/punkt/{lang_nltk}.pickle")
    except Exception:
        sentence_tokenizer = nltk.data.load("tokenizers/punkt/english.pickle")

    # adding additional abbreviations:
    # https://stackoverflow.com/questions/14095971/how-to-tweak-the-nltk-sentence-tokenizer/25375857#25375857
    additional_abbreviations = DICT_ABBREVIATIONS.get(lang, [])
    sentence_tokenizer._params.abbrev_types.update(additional_abbreviations)

    for line in text.split("\n"):
        for sent in sentence_tokenizer.tokenize(line):
            # formatting with whitespaces
            if len(sent) < 2:
                continue
            # remove trailing navigation links to categories etc
            if sent[-1] != ".":
                continue
            # remove captions from images (not handled well by mwparserfromhell)
            # https://github.com/earwig/mwparserfromhell/issues/169
            if "|" in sent:
                continue
            sentences += [sent]
    return sentences


def pad_list(a: list[Any], pad_token: Any = -1, n: int = 15) -> list[Any]:
    """
    Method for list padding
    """
    a = (a + n * [pad_token])[:n]
    return a
