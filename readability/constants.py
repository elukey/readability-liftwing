DICT_ABBREVIATIONS = {
    # https://en.wikipedia.org/wiki/List_of_German_abbreviations
    "de": [
        "am", "amtl", "Anh", "Ank", "Anl", "Anm", "anschl", "a.o", "App", "a.Rh", "Art", "Aufl", "Ausg",
        "b", "Bd", "Bde", "beil", "bes", "bspw", "Best.-Nr", "Betr", "Bev", "Bez", "Bhf", "brit", "b.w", "bzgl", "bzw",
        "ca", "Chr",
        "d.Ä", "dazw", "desgl", "dgl", "d.h", "d.i", "Dipl", "Dipl.-Ing", "Dipl.-Kfm", "Dir", "d.J", "d.M", "d.O",
        "d.o", "Dr", "dt", "Dtzd" \
                           "e.h", "ehem", "eigtl", "einschl", "engl", "entspr", "erb", "Erw", "erw", "ev", "e.V",
        "evtl", "e.Wz", "exkl", \
        "f", "ff", "Fa", "Fam", "Ffm", "Fr", "fr", "Frl", "Frfr", "frz", \
        "geb", "Gebr", "gedr", "gegr", "gek", "Ges", "gesch", "geschl", "geschr", "ges.gesch", "gest", "gez", "ggf",
        "ggfs", \
        "Hbf", "hpts", "Hptst", "Hr", "Hrn", "Hrsg", \
        "i.A", "i.b", "i.b", "i.B", "i.D", "i.H", "i.J", "Ing", "Inh", "inkl", "i.R", "i.V", "inzw", \
        "jew", "Jh", "jhrl", \
        "k.A", "Kap", "kath", "Kfm", "kfm", "kgl", "Kl", "kompl", \
        "l", "led", "L.m.a.A", \
        "m.a.W", "max", "med", "m.E", "Mil", "Mio", "m.M", "m.M.n", "möbl", "Mrd", "Msp", "mtl", "mdl", "m.ü.M", "m.W",
        "MwSt", "Mw-St", \
        "n", "näml", "n.Chr", "n.J", "nördl", "norw", "Nr", "n.u.Z", \
        "o", "o.A", "o.a", "o.ä", "o.B", "od", "o.g", "österr", \
        "p.Adr", "Pfd", "Pkt", "Pl", "phil", \
        "r", "Reg.-Bez", "r.k", "r.-k", "röm.-kath", "röm", \
        "S", "s", "s.a", "Sa", "schles", "schwäb", "schweiz", "sek", "s.o", "sog", "St", "Std", "Str", "s.u", "südd", \
        "tägl", "Tel", \
        "u", "u.a", "u.ä", "u.Ä", "u.a.m", "u.A.w.g", "übl", "üblw", "usw", "u.v.a", "u.v.a.m", "u.U", "u.zw", \
        "v", "V", "v.a", "v.Chr", "Verf", "verh", "verw", "vgl", "v.H", "vorm", "v.R.w", "v.T", "v.u.Z", \
        "wstl", "w.o", \
        "z", "Z", "z.B", "z.Hd", "Zi", "z.T", "Ztr", "zur", "zus", "zzgl", "z.Z", "z.Zt"
    ]
}

# mapping between wikipedia-languages and nltk-languages
DICT_LANG_NLTK = {
    "cs": "czech",
    "da": "danish",
    "de": "german",
    "el": "greek",
    "en": "english",
    "es": "spanish",
    "et": "estonian",
    "fi": "finnish",
    "fr": "french",
    "it": "italian",
    "nl": "dutch",
    "nn": "norwegian",
    "no": "norwegian",
    "simple": "english",
    "pl": "polish",
    "pt": "portuguese",
    "ru": "russian",
    "sl": "slovene",
    "sv": "swedish",
    "tr": "turkish"
}
