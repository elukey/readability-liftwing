import argparse
import pathlib

import joblib  # type: ignore

from catboost import CatBoostRegressor  # type: ignore
from transformers import pipeline  # type: ignore
from transformers import AutoTokenizer, BertForSequenceClassification  # type: ignore

from readability.models.readability_bert.model import MultilingualReadabilityModel


parser = argparse.ArgumentParser(description='Script for creating a model binary')
parser.add_argument('-bm', '--bert', help='Bert model path', required=True)
parser.add_argument('-sm', '--scorer', help='FK-scorer model', required=True)
parser.add_argument('-name', '--name', help='binary name', required=True)

args = vars(parser.parse_args())
model_path = args["bert"]
fk_scorer_path = args["scorer"]
binary_model_path = args["name"]

model_path = pathlib.Path(model_path)
tokenizer = AutoTokenizer.from_pretrained(
    model_path, truncation=True, max_length=512, device="cpu"
)
model = BertForSequenceClassification.from_pretrained(model_path).to("cpu")
model_pipeline = pipeline(task="text-classification", model=model, tokenizer=tokenizer, batch_size=1)
supported_wikis = [
    'af', 'an', 'ar', 'ast', 'azb', 'az', 'bar', 'ba', 'be', 'bg', 'bn', 'bpy', 'br', 'bs', 'ca', 'ceb', 'ce', 'cs',
    'cv', 'cy', 'da', 'de', 'el', 'en', 'es', 'et', 'eu', 'fa', 'fi', 'fr', 'fy', 'ga', 'gl', 'gu', 'he', 'hi', 'hr',
    'ht', 'hu', 'hy', 'id', 'io', 'is', 'it', 'ja', 'jv', 'ka', 'kk', 'kn', 'ko', 'ky', 'la', 'lb', 'lmo', 'lt', 'lv',
    'mg', 'min', 'mk', 'ml', 'mn', 'mr', 'ms', 'my', 'nds_nl', 'ne', 'new', 'nl', 'nn', 'no', 'oc', 'pa', 'pl', 'pms',
    'pnb', 'pt', 'ro', 'ru', 'scn', 'sco', 'sh', 'sk', 'sl', 'sq', 'sr', 'su', 'sv', 'sw', 'ta', 'te', 'tg', 'th',
    'tl', 'tr', 'tt', 'uk', 'ur', 'uz', 'vi', 'vo', 'war', 'yo', 'zh', 'simple'
]

fk_scorer = CatBoostRegressor()
fk_scorer.load_model(fk_scorer_path)


model_instance = MultilingualReadabilityModel(
    model_version=2,
    classifier=model_pipeline,
    fk_scorer=fk_scorer,
    supported_wikis=supported_wikis
)
joblib.dump(model_instance, binary_model_path, compress=9)
