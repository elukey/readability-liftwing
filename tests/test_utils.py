from readability.utils import text2sentences, wikitext2text


def test_wikitext2text() -> None:
    wikitext = """This is a lead.== Section I ==
    Sec I body.
    === Section I.A ===
    Section I.A [[body]].
    """
    target_text = "This is a lead."
    processed_text = wikitext2text(wikitext)
    assert target_text == processed_text


def test_text2sentences() -> None:
    initial_text = "Sentence number one. Sentence number two. Let's go."
    split_text = ["Sentence number one.", "Sentence number two.", "Let's go."]
    split_result = text2sentences(initial_text)
    assert (split_text == split_result)
